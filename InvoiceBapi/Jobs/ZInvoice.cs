﻿using BAPI_INVOICE;
using Dapper;
using InvoiceBapi.Helper;
using InvoiceBapi.Models;
using InvoiceBapi.Persistence;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace InvoiceBapi.Jobs
{
    public class ZInvoice : IInvoice
    {
       
        private readonly IConfiguration _configuration;
       
        public ZInvoice( IConfiguration configuration)
        {
            _configuration = configuration;

        }
        public async Task INVOICE_Load()
        {
           
            try
            {

               // DateTime currentRuntime = DateTime.Now;
                // DateTime lastRunTime = _minetDbContext.Mst_Parameter_MastersDbSet.First().Dist_Ac_Last_Run_Date != null ? (DateTime)_minetDbContext.Mst_Parameter_MastersDbSet.First().Dist_Ac_Last_Run_Date : currentRuntime.Subtract(TimeSpan.FromDays(5000));
                //string ZMATD = currentRuntime.ToString("yyyyMMdd");
                SqlConnection conT = new SqlConnection(_configuration.GetConnectionString("DataConnection"));
                DateTime Inv_Date = await conT.ExecuteScalarAsync<DateTime>("select Inv_Last_Run_Date from MB_Parameter_Master");

                //string ZINVD = Inv_Date.ToString("yyyyMMdd");
                //string ZINVT = Inv_Date.ToString("HH:mm:ss");
                string ZINVD = "20220701";
                string ZINVT = "00:00:00";

                //SqlConnection con = new SqlConnection(_configuration.GetConnectionString("DataConnection"));
                // DateTime currentRuntime = DateTime.Now;
                // currentRuntime = currentRuntime.Subtract(TimeSpan.FromDays(5000));
                // string currentRuntimeN = currentRuntime.ToString("yyyyMMdd");
                //  string lastRunTimeN = lastRunTime != null ? lastRunTime: currentRuntimeN;
                // string ZMATD = currentRuntimeN == lastRunTime ? "20160707" : lastRunTime;


                //  new Util(configuration).SaveExceptionLog("DAS SAP Invoke start from Date::" + ZMATD, "ZMIDAS Service:: DAS", "Process InProgress");

                ZBGLINV_NRequest _request = new ZBGLINV_NRequest(new ZMBLINVDTL_ST[2], new ZMBLINVHDR_ST[2], ZINVD, ZINVT);
                //var DAutonumber = _MblContext.MB_Dist_Ac_StmtBO.OrderByDescending(x => x._id)
                //    .Select(x => x.dAutoNumber)
                //    .FirstOrDefault();
                // var DAutonumber = "656557";
                // Int64 D_Autonumber = Convert.ToString(DAutonumber).IsNotNullOrEmpty() ? Convert.ToInt64("656537") : Convert.ToInt64(DAutonumber);

                // Int64 D_Autonumber = Convert.ToString(DAutonumber).IsNotNullOrEmpty() ? Convert.ToInt64(DAutonumber) : Convert.ToInt64("1");
                //new Util(configuration).SaveExceptionLog("DAS Request Created Successfully", "ZMIDAS Service:: DAS", "Process InProgress");

                // var sapurl = _configuration["SapUrl"];
                // var sapclient = _configuration["SapClient"];
                //var sapuser = _configuration["SapUserName"];
                //var sappassword = _configuration["SapPassword"];
                var sapuser = "HOITBAMGR3";
                var sappassword = "Jan@2023";
                // var sapservices = _configuration["SapServices"];
                // var BapiUrl = sapurl + "?" + "services" + "=" + sapservices + "&" + "sap-client" + "=" + sapclient + "&" + "sap-user" + "=" + sapuser + "&" + "sap-password" + "=" + sappassword;
                // var BapiUrl = sapurl + "?" + "sap-user" + "=" + sapuser + "&" + "sap-password" + "=" + sappassword + "&" + "sap-client" + "=" + sapclient;
                // await new ExceptionHelp(_configuration).SaveException(nameof(ZMIDAS), "BapiURL", BapiUrl);

                ZBGLINV_NPortTypeClient bapi = new ZBGLINV_NPortTypeClient(sapuser, sappassword);
                //await new ExceptionHelp(_configuration).SaveException(nameof(ZMIDAS), "ZMBL_MIDASPortTypeClient", bapi.ToString());

                var _bapiResult = await bapi.ZBGLINV_NAsync(_request);
                //await new ExceptionHelp(_configuration).SaveException(nameof(ZMIDAS), "ZMBL_MIDASAsync", _bapiResult.ToString());

                IList<Invoice_Details> _tableDetailsTypeList = new List<Invoice_Details>();
                int i = 1;
                foreach (var itemsD in _bapiResult.INVOICEDTL)
                {
                    _tableDetailsTypeList.Add(new Invoice_Details
                    {
                        NO = i,
                        Inv_Number = Convert.ToString(itemsD.VBELN).Trim(),
                        LineItem_Number = (itemsD.POSNR.TrimStart('0')),
                        Item_Code = (itemsD.MATNR.TrimStart('0')),
                        Item_Description = Convert.ToString(itemsD.ARKTX).Trim(),
                        Brand_Code = Convert.ToString(itemsD.MATKL).Trim(),
                        Batch_Number = Convert.ToString(itemsD.CHARG).Trim(),
                        Qty_In_SUM = Convert.ToString(itemsD.FKIMG).Trim(),

                        // Qty_In_SUM = Convert.ToString(itemsD.FKIMG).TrimEnd('0').TrimEnd('.'),

                        Sales_UOM = Convert.ToString(itemsD.VRKME).Trim(),
                        // Qty_In_BUM = Convert.ToString(itemsD.ZQBUM).TrimEnd('0').TrimEnd('.'),
                        // Qty_In_SUM = itemsD.FKIMG.ToString().Contains('.') ? itemsD.FKIMG.ToString().TrimStart('0').TrimEnd('0').TrimEnd('.') : itemsD.FKIMG.ToString().TrimStart('0'),

                        Qty_In_BUM = Convert.ToString(itemsD.ZQBUM).Trim(),

                        Qty_In_RUM = Convert.ToString(itemsD.ZQRUM).Trim(),
                        Reporting_UOM = Convert.ToString(itemsD.MEGRU).Trim(),
                        //Unit_MRP_In_BUM = Convert.ToString(itemsD.ZMRP).TrimEnd('0').TrimEnd('.'),
                        Unit_MRP_In_BUM = Convert.ToString(itemsD.ZMRP).Trim(),
                        Unit_Discount = Convert.ToString(itemsD.ZUDIS).Trim(),
                        Discount_UOM = Convert.ToString(itemsD.KMEIN).Trim(),
                        Item_Discount = Convert.ToString(itemsD.ZP_YBPD).Trim(),
                        Item_Freight = Convert.ToString(itemsD.ZFRT).Trim(),
                        Item_Octroi = Convert.ToString(itemsD.ZOCT).Trim(),
                        Item_Ret_Margin = Convert.ToString(itemsD.ZRTMG).Trim(),
                        Item_Net_Value = Convert.ToString(itemsD.NETWR).Trim(),
                        Item_Tax_Amt = Convert.ToString(itemsD.MWSBP).Trim(),
                        Item_Total_Value = Convert.ToString(itemsD.ZVAL).Trim(),
                        Rec_Qty_Good = Convert.ToString(itemsD.ZMATNR.TrimStart('0')),
                        Damaged_Qty = Convert.ToString(itemsD.ZFFLAG).Trim(),
                        Shortage_Excess_Qty = Convert.ToString(itemsD.ZASMSN).Trim(),
                        Free_Item_Flag = Convert.ToString(itemsD.ZFFLAG).Trim(),
                        P_Item_Qty = Convert.ToString(itemsD.ZFKIMG).Trim(),
                        P_Item_UOM = Convert.ToString(itemsD.ZVRKME).Trim(),
                        ASM_Sanction_No = Convert.ToString(itemsD.ZASMSN).Trim(),
                        Dist_Rate = Convert.ToString(itemsD.ZNETP).Trim(),
                        Dist_Margin = Convert.ToString(itemsD.ZDTMG).Trim(),
                        Stockist_Margin = Convert.ToString(itemsD.ZSTMG).Trim(),
                        Second_Pt_Tax = Convert.ToString(itemsD.ZSTAX).Trim(),
                        RSL = Convert.ToString(itemsD.SLIFE).Trim(),
                        Amount_First = Convert.ToString(itemsD.ZV_ZLD1).Trim(),
                        Amount_Second = Convert.ToString(itemsD.ZV_ZLD2).Trim(),
                        Percentage_First = Convert.ToString(itemsD.ZP_ZLD1).Trim(),
                        Percentage_Second = Convert.ToString(itemsD.ZP_ZLD2).Trim(),
                        VAT_Tax_Amt = Convert.ToString(itemsD.ZV_JIN1).Trim(),
                        Amount_VATR = Convert.ToString(itemsD.ZV_ZTOT).Trim(),
                        Percentage_VATR = Convert.ToString(itemsD.ZP_ZTOT).Trim(),
                        item_ret_margin_value = Convert.ToString(itemsD.ZRTMG_VAL).Trim(),
                        AIT_percentage = Convert.ToString(itemsD.ZP_YMST).Trim(),
                        AIT_value = itemsD.ZV_YMST,
                        //AIT_value = float.TryParse(itemsD.ZV_YMST)
                        AdvTLO_percentage = Convert.ToString(itemsD.ZP_YBPE).Trim(),
                        AdvTLO_value = Convert.ToString(itemsD.ZV_YBPE).Trim(),
                        P_Item_Code = Convert.ToString(itemsD.ZMATNR.TrimStart('0')).Trim(),
                        Base_UOM = Convert.ToString(itemsD.MEINS).Trim()
                    }) ;
                    i++;

                }


                // IList<Invoice_headers> _tableDetailsTypeList = new List<Invoice_headers>();

                IList<Invoice_headers> _tableHeaderTypeList = new List<Invoice_headers>();

                int j = 1;


                foreach (var itemsH in _bapiResult.INVOICEHDR)
                {
                    _tableHeaderTypeList.Add(new Invoice_headers
                    {
                        NO = j,
                        Inv_Number = itemsH.VBELN,
                        Inv_Date = itemsH.FKDAT,
                        Inv_Time = (itemsH.FKDAT + " " + itemsH.ERZET),
                        Customer_Code = (itemsH.KUNAG.TrimStart('0')),
                        Payer_Code = (itemsH.KUNRG.TrimStart('0')),
                        Sales_Org = itemsH.VKORG,
                        Transporter_Name = itemsH.ZTRNM,
                        Inv_Amount = Convert.ToString(itemsH.ZTVAL),
                        Inv_Tax = Convert.ToString(itemsH.ZTTAX),
                        Inv_Freight = Convert.ToString(itemsH.ZTFRT),
                        Inv_Octroi = Convert.ToString(itemsH.ZTOCT),
                        Inv_Discount = Convert.ToString(itemsH.ZTDIS),
                        Cancelled_Inv_Number = itemsH.SFAKN,
                        SO_number = itemsH.ZUONR,
                        Doc_type = itemsH.FKART,
                        Bill_date = Convert.ToString(itemsH.ERDAT) == "0000-00-00" ? null : Convert.ToString(itemsH.ERDAT)
                       // Bill_date=null


                    }) ;
                    j++;
                   
                }

                DataTable dtHTT = Util.ToDataTable(_tableHeaderTypeList.ToList());
                // var Ncount = _bapiResult.ACCOUNTDTL.Count();
                //// new Util(configuration).SaveExceptionLog("DAS Result Get Successfully", "ZMIDAS Service:: DAS", "Process InProgress");


                //IList<ZMIDAS_DAS_TT> _tableTypeList = new List<ZMIDAS_DAS_TT>();
                //int i = 1;
                //foreach (var items in _bapiResult.ACCOUNTDTL)
                //{
                //    _tableTypeList.Add(new ZMIDAS_DAS_TT
                //    {
                //        NO = i,
                //        Customer_Code = Convert.ToString(items.KUNNR.TrimStart('0')).Replace("'", "^"),
                //        BUZEI = Convert.ToString(items.BUZEI).Replace("'", "^"),
                //        Document_Status = Convert.ToString(items.ZSTAT).Replace("'", "^"),
                //        Assignment = Convert.ToString(items.ZUONR).Replace("'", "^"),
                //        Document_Number = Convert.ToString(items.BELNR).Replace("'", "^"),
                //        Document_Type = Convert.ToString(items.BLART).Replace("'", "^"),
                //        Document_Type_Desc = Convert.ToString(items.LTEXT).Replace("'", "^"),
                //        Document_Date = Convert.ToString(items.BLDAT).Replace("'", "^"),
                //        Posting_Date = Convert.ToString(items.BUDAT).Replace("'", "^"),
                //        Special_GL_Indicator = Convert.ToString(items.UMSKZ).Replace("'", "^"),
                //        Special_GL_Ind_Text = Convert.ToString(items.ZLTEXT).Replace("'", "^"),
                //        Reference = Convert.ToString(items.XBLNR).Replace("'", "^"),
                //        Net_Due_Date = Convert.ToString(items.ZNETD).Replace("'", "^"),
                //        Net_Due_Status = Convert.ToString(items.ZNETS).Replace("'", "^"),
                //        Document_Amt = Convert.ToString(items.DMSHB).Replace("'", "^"),
                //        Document_Currency = Convert.ToString(items.HWAER).Replace("'", "^"),
                //        Clrg_Doc_No = Convert.ToString(items.AUGBL).Replace("'", "^"),
                //        Clrg_Doc_Date = Convert.ToString(items.AUGDT).Replace("'", "^") == "0000-00-00" ? null : Convert.ToString(items.AUGDT).Replace("'", "^"),
                //        Document_Text = Convert.ToString(items.SGTXT).Replace("'", "^"),
                //        Ref_Key_1 = Convert.ToString(items.XREF1).Replace("'", "^"),
                //        Vendor_Code = Convert.ToString(items.LIFNR).Replace("'", "^"),
                //        D_Autonumber = Convert.ToString(D_Autonumber + i)
                //    });
                //    i++;
                //    // D_Autonumber = D_Autonumber + 1;
                //    // D_Autonumber++;
                //}

                DynamicParameters dataH = new DynamicParameters();

                DataTable dtDTT = Util.ToDataTable(_tableDetailsTypeList.ToList());

                ////Hashtable hash = new Hashtable();
                ////hash.Add("@table", dtTT);
                dataH.Add("@table", dtHTT.AsTableValuedParameter("Invoice_headers"));
               
                //using (SqlConnection con = new SqlConnection(_configuration.GetConnectionString("DataConnection")))
                //{
                    //    //con.(StoreProcedure.Get_Dist_Ac_Statement_Update, hash);
                    // var Nresult = con.Query<Task>("Get_Dist_Ac_Statement_Update", hash, commandType: CommandType.StoredProcedure).ToList();
                   // var Nresult = await con.QueryAsync<DBResponse>("Invoice_headers_Update", dataH,commandTimeout:240, commandType: CommandType.StoredProcedure);
                   // var NDresult = await con.QueryAsync<DBResponse>("Invoice_Details_Update", data, commandTimeout: 240, commandType: CommandType.StoredProcedure);
                    //    // Int32 count = 0;
                    //    // var storedprocedure = "Get_Dist_Ac_Statement_Update";
                    //    // var Nresult= SqlMapper.ExecuteReaderAsync(con, storedprocedure, hash, commandType: CommandType.StoredProcedure);
                    //    // var count =  con.ExecuteAsync("[dbo].[Get_Dist_Ac_Statement_Update]", hash, commandType: CommandType.StoredProcedure);

                    //var Nresult = await con.QueryAsync<DBResponse>();


                    //if (Nresult.Count() < 0)
                    //{
                    //    //        // new Util(configuration).SaveExceptionLog("DataTable Result Is NULL", "ZMIDAS Service:: DAS", "Exception");
                    //}
                    //if (NDresult.Count() < 0)
                    //{
                    //    //        // new Util(configuration).SaveExceptionLog("DataTable Result Is NULL", "ZMIDAS Service:: DAS", "Exception");
                    //}
                    //    else
                    //    {
                    //        var NDBresult = Nresult.ToList();
                    //        var status = NDBresult[0].statusCode;
                    //        var message = NDBresult[0].message;
                    //        if (status == 200)
                    //        {
                    //            // new Util(configuration).SaveExceptionLog("DAS data updated Successfully", "ZMIDAS Service:: DAS", "DAS Invokation Successfully Implement");
                    //            return;
                    //        }
                    //        else
                    //        {
                    //            // new Util(configuration).SaveExceptionLog("DAS throws Error: " + status + ", exception:: " + message, "ZMIDAS Service:: DAS", "DAS Invokation Failed");
                    //            throw (new Exception("SP Failure" + status + " " + message));
                    //        }

                    //    }
                //}
                DynamicParameters dataD = new DynamicParameters();
                dataD.Add("@table", dtDTT.AsTableValuedParameter("Invoice_Details"));
                //Hashtable hash = new Hashtable();
                //hash.Add("@table", dtDTT);

                using (SqlConnection con = new SqlConnection(_configuration.GetConnectionString("DataConnection")))
                {
                    //    //con.(StoreProcedure.Get_Dist_Ac_Statement_Update, hash);
                    // var Nresult = con.Query<Task>("Get_Dist_Ac_Statement_Update", hash, commandType: CommandType.StoredProcedure).ToList();
                   // var Nresult = await con.QueryAsync<DBResponse>("Invoice_headers_Update", data, commandTimeout: 240, commandType: CommandType.StoredProcedure);
                     var NDresult = await con.QueryAsync<DBResponse>("Invoice_Details_Update", dataD, commandTimeout: 240, commandType: CommandType.StoredProcedure);
                    //    // Int32 count = 0;
                    //    // var storedprocedure = "Get_Dist_Ac_Statement_Update";
                    //    // var Nresult= SqlMapper.ExecuteReaderAsync(con, storedprocedure, hash, commandType: CommandType.StoredProcedure);
                    //    // var count =  con.ExecuteAsync("[dbo].[Get_Dist_Ac_Statement_Update]", hash, commandType: CommandType.StoredProcedure);

                    //var Nresult = await con.QueryAsync<DBResponse>();


                    //if (Nresult.Count() < 0)
                    //{
                    //    //        // new Util(configuration).SaveExceptionLog("DataTable Result Is NULL", "ZMIDAS Service:: DAS", "Exception");
                    //}
                    if (NDresult.Count() < 0)
                    {
                        //        // new Util(configuration).SaveExceptionLog("DataTable Result Is NULL", "ZMIDAS Service:: DAS", "Exception");
                    }
                    //    else
                    //    {
                    //        var NDBresult = Nresult.ToList();
                    //        var status = NDBresult[0].statusCode;
                    //        var message = NDBresult[0].message;
                    //        if (status == 200)
                    //        {
                    //            // new Util(configuration).SaveExceptionLog("DAS data updated Successfully", "ZMIDAS Service:: DAS", "DAS Invokation Successfully Implement");
                    //            return;
                    //        }
                    //        else
                    //        {
                    //            // new Util(configuration).SaveExceptionLog("DAS throws Error: " + status + ", exception:: " + message, "ZMIDAS Service:: DAS", "DAS Invokation Failed");
                    //            throw (new Exception("SP Failure" + status + " " + message));
                    //        }

                    //    }
                }
                Console.WriteLine(dtDTT);

            }
                catch (Exception ex)
                {
                // new Util(configuration).SaveExceptionLog(ex.Message.ToString(), "ZMIDAS Service:: DAS", ex.GetType().Name.ToString());
                //throw (ex);
                var a = "sdfs ds";
                }

            }

          
    }
}
