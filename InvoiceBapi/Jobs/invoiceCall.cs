﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace InvoiceBapi.Jobs
{
    public class invoiceCall: IHostedService
    {

        private readonly IConfiguration _configuration;

        public invoiceCall( IConfiguration configuration)
        {

            _configuration = configuration;

        }
        public async Task executeJOB()
        {
            try
            {
                ZInvoice _result = new ZInvoice(_configuration);
                await _result.INVOICE_Load();

            }
            catch (Exception ex)
            {
                // _logger.LogError(ex, "Exception in Post Population for DistributorAccountStatement_Bapi");
                // new Util(_configuration).SaveExceptionLog(ex.Message.ToString(), "DistributorAccountStatement_Bapi", ex.GetType().ToString());
                throw ex;

            }
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            ZInvoice _result = new ZInvoice(_configuration);
           var result = _result.INVOICE_Load();
            return result;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
