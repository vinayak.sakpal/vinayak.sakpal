﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InvoiceBapi.Models
{
    public class Invoice_Details
    {
		public int NO { get; set; }
		public string Inv_Number { get; set; }
		public string LineItem_Number { get; set; }
		public string Item_Code { get; set; }
		public string Item_Description { get; set; }
		public string Brand_Code { get; set; }
		public string Batch_Number { get; set; }
		public string Qty_In_SUM { get; set; }
		public string Sales_UOM { get; set; }
		public string Qty_In_BUM { get; set; }
		public string Qty_In_RUM { get; set; }
		public string Reporting_UOM { get; set; }
		public string Unit_MRP_In_BUM { get; set; }
		public string Unit_Discount { get; set; }
		public string Discount_UOM { get; set; }
		public string Item_Discount { get; set; }
		public string Item_Freight { get; set; }
		public string Item_Octroi { get; set; }
		public string Item_Ret_Margin { get; set; }
		public string Item_Net_Value { get; set; }
		public string Item_Tax_Amt { get; set; }
		public string Item_Total_Value { get; set; }
		public string Rec_Qty_Good { get; set; }
		public string Damaged_Qty { get; set; }
		public string Shortage_Excess_Qty { get; set; }
		public string Free_Item_Flag { get; set; }
		public string P_Item_Qty { get; set; }
		public string P_Item_UOM { get; set; }
		public string Dist_Margin { get; set; }
		public string Stockist_Margin { get; set; }
		public string Second_Pt_Tax { get; set; }
		public string RSL { get; set; }
		public string Amount_First { get; set; }
		public string Amount_Second { get; set; }
		public string Percentage_First { get; set; }
		public string Percentage_Second { get; set; }
		public string VAT_Tax_Amt { get; set; }
		public string Amount_VATR { get; set; }
		public string Percentage_VATR { get; set; }
		public string item_ret_margin_value { get; set; }
		public string AIT_percentage { get; set; }
		public decimal AIT_value { get; set; }
		public string AdvTLO_percentage { get; set; }
		public string AdvTLO_value { get; set; }
        public string ASM_Sanction_No { get; set; }
        public string Dist_Rate { get; set; }
        public string P_Item_Code { get; set; }
        public string Base_UOM { get; set; }
    }
}
