﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InvoiceBapi.Models
{
    public class Invoice_headers
    {
        public int NO { get; set; }
        public string Inv_Number { get; set; }
        public string Inv_Date { get; set; }
        public string Inv_Time  { get; set; }
        public string Customer_Code { get; set; }
        public string Payer_Code { get; set; }
        public string Sales_Org { get; set; }
        public string Transporter_Name { get; set; }
        public string Inv_Amount { get; set; }
        public string Inv_Tax { get; set; }
        public string Inv_Freight { get; set; }
        public string Inv_Octroi { get; set; }
        public string Inv_Discount { get; set; }
        public string Cancelled_Inv_Number { get; set; }
        public string? Bill_date { get; set; } 
        public string SO_number { get; set; }
        public string Doc_type { get; set; }
        
    }
}
