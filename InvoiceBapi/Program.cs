using InvoiceBapi.Jobs;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InvoiceBapi
{
    public class Program
    {
        private static IConfiguration _configuration;

        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
            invoiceCall Result = new invoiceCall(_configuration);
            Result.executeJOB();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
